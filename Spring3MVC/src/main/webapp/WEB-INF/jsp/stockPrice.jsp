<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
   <center>
   <form action="getData">
    <p>Please Enter Stock code.<br>
    <br><input type="text" name="stockCode" >
    <p>Please Enter currency.<br>
    <br><br><input type="text" name="currency" >
    <br><br><br><input type="submit">
   </form>
   
   <c:choose>
   <c:when test="${success!=null}">
   <div>
   <center>Success Response</center>
     <table>
      <tr>
     <td>Stock Code</td>
     <td>Stock Price(USD)</td>
     <td>Stock Price(INR)</td>
     </tr>
     <tr>
     <td>${success.stockCode}</td>
     <td>${success.stockPriceInUSD}</td>
     <td>${success.stockPriceInINR}</td>
     </tr>
     </table>
   </div>
   </c:when>
   <c:otherwise>
   <table>
   <center>Error Response</center>
      <tr>
     <td>Error Code</td>
     <td>Message</td>
    
     </tr>
     <tr>
     <td>${error.errorCode}</td>
     <td>${error.message}</td>
     
     </tr>
     </table>
   
   </c:otherwise>
   </c:choose>
   
   
   </center>


</body>
</html>