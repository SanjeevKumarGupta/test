package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.test.model.ErrorResponse;
import com.test.model.RequestModel;
import com.test.model.SuccessResponse;
import com.test.service.MVCService;

@Controller
@RequestMapping("/welcome")
public class MVCController {

	@Autowired
	private MVCService MVCService;

	@RequestMapping(value = "/")
	public String getDefaultPage() {
		return "stockPrice";
	}

	@RequestMapping(value = "/getData")
	public String getStockValue(RequestModel RequestModel, Model model) {

		Object obj = MVCService.getRestData(RequestModel);
		if (obj instanceof SuccessResponse) {
			model.addAttribute("success", (SuccessResponse) obj);
		} else {
			model.addAttribute("error", (ErrorResponse) obj);
		}
		return "stockPrice";
	}

}
