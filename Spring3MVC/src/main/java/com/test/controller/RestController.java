package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.service.RestService;

@Controller
@RequestMapping("/stock")
public class RestController {

	@Autowired
	private RestService restService;

	
	@RequestMapping(value = "/{stockCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getSTockPriceDefaultCurrency(@PathVariable String stockCode) {

		Object obj = restService.getStockResponse(stockCode, null);
		return obj;

	}

	@RequestMapping(value = "/{stockCode}/{currency}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getSTockPriceWithCurrency(@PathVariable String stockCode,
			@PathVariable String currency) {

		Object obj = restService.getStockResponse(stockCode, currency);
		return obj;

	}
}