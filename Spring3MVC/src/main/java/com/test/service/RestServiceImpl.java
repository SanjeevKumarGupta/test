package com.test.service;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.test.model.ErrorResponse;
import com.test.model.SuccessResponse;
import com.test.util.DataObject;

@Service
public class RestServiceImpl implements RestService {

	public Object getStockResponse(String stockCode, String currency) {
		if (stockCode == null || stockCode.length() < 4)
			return getErrorObject();

		Object obj = null;
		DataObject dataObject = new DataObject();

		Map<String, BigDecimal> stockMap = dataObject.getAllStockMap();
		BigDecimal stockValueInUSD = stockMap.get(stockCode.toUpperCase());

		if (stockCode != null && stockValueInUSD != null) {
			BigDecimal stockValueInINR = dataObject.getStockValueInINR(stockValueInUSD);

			SuccessResponse sucessResponse = new SuccessResponse();
			sucessResponse.setStockCode(stockCode);
			if (currency != null) {
				sucessResponse.setStockPriceInINR(stockValueInINR);
			}
			sucessResponse.setStockPriceInUSD(stockValueInUSD);
			obj = sucessResponse;

		} else {

			obj = getErrorObject();
		}

		return obj;
	}

	public ErrorResponse getErrorObject() {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(10000);
		error.setMessage("Stock Code is not valid");
		return error;
	}
}
