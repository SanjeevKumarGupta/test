package com.test.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import com.test.model.ErrorResponse;
import com.test.model.RequestModel;
import com.test.model.SuccessResponse;

@Service
public class MVCServiceImpl implements MVCService {

	public Object getRestData(RequestModel model) {
		if (model.getStockCode() != null && model.getStockCode().length() == 4) {
			return this.processGETcall(model.getStockCode(), model.getCurrency());
		} else {
			return getErrorObject();
		}
	}

	public Object processGETcall(String stockCode, String currency) {
		HttpResponse finalHttpResponse = null;
		Object obj = null;
		String strUrl = "http://localhost:8080/SpringMVC/stock/" + stockCode;
		if (currency != null) {
			strUrl = strUrl + "/" + currency;
		}
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(strUrl);
			finalHttpResponse = client.execute(httpGet);
			if (finalHttpResponse != null) {
				String currentLine = null, readAPIresponse = "";
				if (finalHttpResponse.getStatusLine().getStatusCode() == 200) {

					SuccessResponse response = new SuccessResponse();
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(finalHttpResponse.getEntity().getContent()));

					while ((currentLine = bufferedReader.readLine()) != null) {
						readAPIresponse = readAPIresponse + currentLine;
					}

					ObjectMapper objectMapper = new ObjectMapper();
					response = objectMapper.readValue(readAPIresponse, SuccessResponse.class);
					
					obj = response;
				} else {
					ErrorResponse errorResponse = new ErrorResponse();
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(finalHttpResponse.getEntity().getContent()));

					while ((currentLine = bufferedReader.readLine()) != null) {
						readAPIresponse = readAPIresponse + currentLine;
					}

					ObjectMapper objectMapper = new ObjectMapper();
					errorResponse = objectMapper.readValue(readAPIresponse, ErrorResponse.class);
					obj = errorResponse;
				}

			}
		} catch (Exception e) {

			e.getMessage();
		}
		return obj;
	}

	public ErrorResponse getErrorObject() {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(10000);
		error.setMessage("Stock Code is not valid");
		return error;
	}
}
