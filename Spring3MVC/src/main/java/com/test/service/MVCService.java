package com.test.service;

import com.test.model.RequestModel;

public interface MVCService {
	public Object getRestData(RequestModel model);
}
