package com.test.service;

public interface RestService {

	public Object getStockResponse(String stockCode, String currency);
}
