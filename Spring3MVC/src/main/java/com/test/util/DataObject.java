package com.test.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class DataObject {

	// Data of stock where map key is stock code and value is stock price in USD

	public Map<String, BigDecimal> getAllStockMap() {

		
		Map<String, BigDecimal> dataMap = new HashMap<String, BigDecimal>();
		dataMap.put("ABCD", new BigDecimal("100"));
		dataMap.put("ABDE", new BigDecimal("100"));
		dataMap.put("ABEF", new BigDecimal("100"));
		dataMap.put("ABGG", new BigDecimal("100"));
		dataMap.put("ABFH", new BigDecimal("100"));
		dataMap.put("ABGI", new BigDecimal("100"));
		dataMap.put("ABGJ", new BigDecimal("100"));
		dataMap.put("ABGK", new BigDecimal("100"));

		return dataMap;
	}

	public BigDecimal getStockValueInINR(BigDecimal stockPriceInDoller) {
		BigDecimal stockPriceInINR = stockPriceInDoller.divide(new BigDecimal(50));
		return stockPriceInINR;

	}
   
	
	
}
