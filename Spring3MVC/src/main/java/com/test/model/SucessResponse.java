package com.test.model;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SucessResponse {

	private String stockCode;
	private BigDecimal stockPriceInUSD;
	private BigDecimal stockPriceInINR;
	private String currencyCode;
	
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public BigDecimal getStockPriceInUSD() {
		return stockPriceInUSD;
	}
	@JsonProperty("StockPriceInUSD")
	public void setStockPriceInUSD(BigDecimal stockPriceInUSD) {
		this.stockPriceInUSD = stockPriceInUSD;
	}
	public BigDecimal getStockPriceInINR() {
		return stockPriceInINR;
	}
	@JsonProperty("StockPriceInINR")
	public void setStockPriceInINR(BigDecimal stockPriceInINR) {
		this.stockPriceInINR = stockPriceInINR;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	

}
